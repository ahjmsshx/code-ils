ILS - Release V1
----------------
Relates to the paper : Learning an Invariant Hilbert Space for Domain Adaptation by Samitha Herath, Mehrtash Harandi and Fatih Porikli
Code provided by : Samitha Herath (samitha.herath@data61.csiro.au)

Instructions :

Step 1 . Data processing

i)   Place the vectorized domain features in the '/Data/Features' directory (if testing on your own data).
ii)  The data processing parameters are set by the function 'dataSetupParameters.m'
iii) run the 'PrepareData.m' matlab code file.

Step 2 . Running ILS

i)   The parameters of ILS is set with the function 'setupParameters.m'.
ii)  run the 'Demo.m' matlab code file.

With this code we package the preprocessed VGG-Net features. The code is based on our unsupervised(US) DA experiments set.

The code uses the 'Manopt' matlab toolbox. However, we package the required functions from manopt for ILS.


If you use this code please cite our paper, Learning an Invariant Hilbert Space for Domain Adaptation :



and manopt matlab toolbox:

@article{manopt,
  author  = {Boumal, N. and Mishra, B. and Absil, P.-A. and Sepulchre, R.},
  title   = {{M}anopt, a {M}atlab Toolbox for Optimization on Manifolds},
  journal = {Journal of Machine Learning Research},
  year    = {2014},
  volume  = {15},
  pages   = {1455--1459},
  url     = {http://www.manopt.org}
}


Note that this code is the research code and soon a cleaned up version will be provided. 
 
The code provided on this repository should only be used for academic purposes.




