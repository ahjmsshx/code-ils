% ILS - Release V1
% ----------------
% Related with the paper : Learning an Invariant Hilbert Space for Domain Adaptation by Samitha Herath, Mehrtash Harandi and Fatih Porikli
% Code provided by : Samitha Herath (samitha.herath@data61.csiro.au)
%(This code should only be used for academic purposes)
clear global
close all
clear
clc

addpath([pwd '/Functions'])
addpath([pwd '/Functions/manopt'])
warning('off','all')

global trainData Sx Xm
global simPairInds difPairInds 
global M W                 
global U L Vs Vd Mp lamda1 beta Th

ParameterSetup = setupParameters();

dataDir = [pwd '/Data'];

lamda1  = ParameterSetup.lamda;
Th      = 250; 

n = ParameterSetup.n; 
p = ParameterSetup.p; 

iters       = ParameterSetup.iters;
NSplits     = ParameterSetup.NSplits;
domainSets  = defineDomainSets(ParameterSetup.domainNames);

domainSetAccs = [];
domainSetSTDs = [];
for dd = 1 : size(domainSets,2)
    
    domainSet = domainSets{dd};
    sourceName = domainSet{1};
    targetName = domainSet{2};
    
    pairedSplitsDir = [dataDir '/PairedData/' sourceName '-' targetName ];
    
    splitTestAcc = [];
    for split = 1 : NSplits
        
        load([pairedSplitsDir '/PairedSplit_' num2str(split)])
        
        Xm{1} = mean(trainData.X{1},2);
        Xm{2} = mean(testData.X{2},2);
        
        Pr = princomp([trainData.X{1} testData.X{2}]');
        W{1} = Pr(:,1:p);
        W{2} = Pr(:,1:p);
        
        % Covariance of domains--------------------------------------------
        X{1} = bsxfun(@minus,trainData.X{1},Xm{1});
        X{2} = bsxfun(@minus,testData.X{2},Xm{2});
        
        Sx{1} = X{1}*X{1}'/(size(X{1},2) - 1);
        Sx{2} = X{2}*X{2}'/(size(X{2},2) - 1);
   
        trainData.X{1} = bsxfun(@minus,trainData.X{1},Xm{1});
        testData.X{2}  = bsxfun(@minus,testData.X{2},Xm{2});
        
        Mp =  eye(p);
        M  =  eye(p);
        
        % Initialization of margins----------------------------------------

        u = 1e-2; l = 1e-2;
        initializeConstraints(u,l)
        Vs = logStruct(L);
        Vd = logStruct(U);

        u = 1; l = 1;
        initializeConstraints(u,l)

        % Define Problems--------------------------------------------------
        epsilon = 1e-5;
        options.tolgradnorm = epsilon; 
        options.maxiter = 5; 
        options.minstepsize = 1e-10;
        
        switch ParameterSetup.manifold
            case 'stiefel'
                manifoldW = stiefelfactory(n,p);
            case 'euclidean'
                manifoldW = euclideanfactory(n,p);
            otherwise
                clc
                disp('no such manifold!')
                return
        end
        
        % Problem 1
        problem1.M     = manifoldW;
        problem1.cost  = @cost1;
        problem1.egrad = @grad1;
       
        % Problem 2
        problem2.M     = manifoldW;
        problem2.cost  = @cost2;
        problem2.egrad = @grad2;
        
        % Problem 4
        manifoldM      = sympositivedefinitefactory(p);
        problem4.M     = manifoldM;
        problem4.cost  = @cost4;
        problem4.egrad = @grad4;
        
        % Optimization
        iter = 1;
        while iter < iters

            clc
            disp([sourceName '->' targetName '(Split :' num2str(split) ')'])
            disp(['Iteration : ' num2str(iter)])
            disp('---------------')
            
            beta = estimateBeta();

            W{1} = steepestdescent(problem1,W{1},options);
            W{2} = steepestdescent(problem2,W{2},options);
            M    = steepestdescent(problem4,M,options);
            slackVectorUpdate(options);
                        
            iter = iter + 1;
            
        end
        
        splitTestAcc(split) = optNNAccuracy(testData);

    end
    
    domainSetAccs(dd) = mean(splitTestAcc);
    domainSetSTDs(dd) = std(splitTestAcc)/sqrt(NSplits);
        
end

% Display Results ---------------------------------------------------------
clc
for dd = 1 : size(domainSets,2)
    
    domainSet = domainSets{dd};
   
    sourceName = domainSet{1};
    targetName = domainSet{2};
    
    disp('---------------------------------')
    disp([sourceName '  -------->  ' targetName])
    disp(['Accuracy :' num2str(domainSetAccs(dd)) ' %' '(' num2str(domainSetSTDs(dd)) ')'])
    disp('---------------------------------')
    
end

disp(['Mean Acc.:' num2str(mean(domainSetAccs)) '%'])
